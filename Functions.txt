##List of PYTHON built-in functions
1. print() #prints stuff to screen
2. input() #asks for input
3. len() #finds lenght of characters
4. replace() #replaces all the characters inside a data set
5. find() #finds the first letter inside a data set
6. range() #finds range
7. (listname).append #adds a character or value to lists
8. (listname).clear #makes the list empty
9. type() #finds what type of value it is
10. str() #turns variable into a string
11. int() #turns variable into an int
12. (listname).sort() #sorts a list from lowest to greatest
13. (listname).reverse() #sorts a list from highest to lowest
14. (listname).insert(2,200) #adds the character "200" into a list after the second data entry.
15. (listname).pop() # removes the last character from the list